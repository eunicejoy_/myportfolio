<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Eunice Joy</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
       <link rel="stylesheet" href="{{asset('css/app.css')}}">
       <link rel="stylesheet" href="{{asset('css/style.css')}}">

    </head>
    <body>
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
          <a class="navbar-brand" href="#"><img src="{{asset('img/mylogo.png')}}" width="50" height="50" alt="logo"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#welcome-section">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#projects">Works</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">Contact</a>
              </li>
            </ul>
          </div>
        </nav>

        <div id="welcome-section" class="intro">
          <h1>Hey I am Eunice Joy.</h1>
          <p>a web developer</p>
        
        </div>
        
        <div id="projects" class="work">
          <h2 class="work-header">These are some of my projects..</h2>
          <a href="https://codepen.io/joycodes/full/oaJRWw/" target="_blank" class="project project-tile">
            <img class="project-pic" src="https://cloud.githubusercontent.com/assets/15967809/17642794/d084d718-6171-11e6-83fa-ede5d0a67ad2.png" alt="project">
            <div class="project-title">Tribute Page</div>
          </a>
          <a href="https://codepen.io/joycodes/full/VVYbRG" target="_blank" class="project project-tile">
            <img class="project-pic" src="https://preview.ibb.co/gyzWTL/Screenshot-17.png" alt="project">
            <div class="project-title">Technical Documentation Page</div>
          </a>
          <a href="https://codepen.io/joycodes/" class="show-all" target="_blank">Show all</a>
        </div>

        <div id="contact" class="contact">
          <div class="header">
            <h1>Let's work together...</h1>
            <p>How do you take your coffee?</p>
          </div>
          <a href="https://instagram.com/eunicejoy_" target="_blank" class="contact-details">Instagram</a>
          <a id='profile-link' href="https://github.com/eunicejoy" target="_blank" class="contact-details">GitHub</a>
          <a href="https://twitter.com/eunicejoy_" target="_blank" class="contact-details">Twitter</a>
          <a href="mailto:epostrado28@gmail.com" class="contact-details">Send a mail</a>
          <a href="#" class="contact-details">Call me</a>
        </div>

      </div>
    </body>

    {{-- Script --}}
    <script src="{{ asset('js/app.js') }}" defer></script>
</html>
